# Dacodes Backend Test

API documentation is available at: https://documenter.getpostman.com/view/13441081/TVenf8iL

## Use public API
A live instance of the API is available at this URL: https://crcscloud.ddns.net/dacodes-backend-test/api
It is hosted on a t2.micro AWS EC2 instance with an NGINX reverse proxy. Both the API and NGINX are running as containerized services with Docker.

## Run locally
Create the ```.env``` file:

```cp .env.local.example .env```

The Node.js API requires a MongoDB database, if you already have installed MongoDB, it is only required to modify the ```.env``` to add the __readWrite__ user and the database (collection) name.

If you dont have MongoDB a script to run a Docker container is included ```run_mongo.sh```, with the prerequisites being to have Docker installed (the script may require to be run with sudo). For this case it is not required to modify the ```.env``` file.

Finally the Node.js API can be run with:

```npm i && npm run server```

## Frameworks and libraries
* bcrypt: It performs a one-way hash to a string and compares the plain text string to the hash. This is especially useful for storing user passwords in a database. It is a bad practice to keep them in plain text.
* compression: It compresses the JSON responses increasing the web application's performance; it is considered one of the best practices for Node.js web applications.
* cors: It enables cross-origin resource sharing, restricting access to the API; it is considered one of the best practices for web applications. In our case, it may be unnecessary, but I added it out of habit.
* dotenv: It is a bad practice to have hardcoded values in the source code, this allows to use a ```.env``` file to store parameters. 
* express: It is a widely used web framework for Node.js; I selected it as I'm experienced with it.
* express-validator: In my current work, I use a tailored made validator for requests. I selected this validator as it has very nice documentation; joi seems to be another good alternative to validate request parameters.
* helmet: Secures the express application, I always use it with express.
* jsonwebtoken: I needed some token; the API doesn't bother with the token's content, but it may be useful to ease front-end development in the future.
* module-alias: Allows using aliases in the source code for module locations instead of using relative paths.
* mongodb: For the proof of concept and to allow an agile development, a document-based database was used to illustrate the system's operation. In case of going to production, it can be considered to pass it to an SQL structure with ACID properties.
* morgan: It is an HTTP request logger, very useful for web applications.
* uuid: Required some unique id for the resources, I didn't use the one provided by MongoDB as some resources are nested in the documents.
