#!/bin/sh

sed -i 's|ENV_DATABASE_MONGODB_HOST|'$DATABASE_MONGODB_HOST'|g' /app/.env && \
sed -i 's|ENV_DATABASE_MONGODB_PORT|'$DATABASE_MONGODB_PORT'|g' /app/.env && \
sed -i 's|ENV_DATABASE_MONGODB_USER|'$DATABASE_MONGODB_USER'|g' /app/.env && \
sed -i 's|ENV_DATABASE_MONGODB_PASSWORD|'$DATABASE_MONGODB_PASSWORD'|g' /app/.env && \
sed -i 's|ENV_DATABASE_MONGODB_DATABASE|'$DATABASE_MONGODB_DATABASE'|g' /app/.env && \
sed -i 's|ENV_JWT_PRIVATE_KEY|'$JWT_PRIVATE_KEY'|g' /app/.env && \
node /app/index.js 1100
