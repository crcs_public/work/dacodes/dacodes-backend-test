'use strcit';

require('dotenv').config();
require('module-alias/register');
const express = require('express');

const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const router = require('./router.js');

const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use((err, req, res, next) => {
  if (err instanceof SyntaxError) {
    res.status(400).json({message: 'Malformed body.'});
  } else {
    next();
  }
});
app.use(compression());
app.use(cors({
  origin: '*',
  methods: [
    'DELETE',
    'GET',
    'PATCH',
    'POST',
    'PUT',
  ],
  allowedHeaders: ['Content-Type', 'Authorization'],
}));
app.use(helmet());
app.use(morgan('dev'));
app.use(router);

const port = process.argv.slice(2)[0];

app.listen(port, () => {
  console.log(`API running on port ${port}`);
});

