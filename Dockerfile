FROM node:15.0.1-alpine3.12

RUN mkdir -p /app

WORKDIR /app
COPY package.json /app/package.json
RUN npm install

COPY src /app/src
COPY index.js /app/index.js
COPY router.js /app/router.js
COPY .env.example /app/.env

COPY docker/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
