'use strict';

const router = require('express').Router(); // eslint-disable-line new-cap
const {validateToken} = require('@middlewares');

const Courses = require('@domains/courses/resources/courses.resource.js');
const Lessons = require('@domains/lessons/resources/lessons.resource.js');
const Questions = require('@domains/questions/resources/questions.resource.js');
const Users = require('@domains/users/resources/users.resource.js');

router.use('/courses', validateToken, Courses);
router.use('/lessons', validateToken, Lessons);
router.use('/questions', validateToken, Questions);
router.use('/users', Users);

module.exports = router;
