'use strict';

const router = require('express').Router(); // eslint-disable-line new-cap
const {body, validationResult} = require('express-validator');

const {
  validateProfessor,
  validateCourseProfessor,
} = require('@middlewares');

const {
  professorRegisterQuestion,
  professorGetAllLessonQuestions,
  professorGetOneLessonQuestion,
  professorUpdateQuestion,
  professorDeleteQuestion,
} = require('../controllers/questions.controller.js');

router.post('/:courseUuid/:lessonUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
  body('answers').isArray(),
  body('options').isArray(),
  body('score').isInt({min: 0}),
  body('text').isString(),
  body('type').isIn([
    'boolean',
    'multiple-all-answer',
    'multiple-one-answer',
    'one-answer',
  ]),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    professorRegisterQuestion(
        req.appended.course,
        req.params.lessonUuid,
        req.body)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.get('/:courseUuid/:lessonUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  professorGetAllLessonQuestions(
      req.appended.course,
      req.params.lessonUuid)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

router.get('/:courseUuid/:lessonUuid/:questionUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  professorGetOneLessonQuestion(
      req.appended.course,
      req.params.lessonUuid,
      req.params.questionUuid)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

router.put('/:courseUuid/:lessonUuid/:questionUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
  body('answers').isArray(),
  body('options').isArray(),
  body('score').isInt({min: 0}),
  body('text').isString(),
  body('type').isIn([
    'boolean',
    'multiple-all-answer',
    'multiple-one-answer',
    'one-answer',
  ]),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    req.body.uuid = req.params.questionUuid;
    professorUpdateQuestion(
        req.appended.course,
        req.params.lessonUuid,
        req.body)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.delete('/:courseUuid/:lessonUuid/:questionUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  professorDeleteQuestion(
      req.appended.course,
      req.params.lessonUuid,
      req.params.questionUuid)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

module.exports = router;
