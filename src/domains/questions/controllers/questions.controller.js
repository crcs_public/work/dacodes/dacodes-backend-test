'use strict';

const {Question} = require('@models');

const professorRegisterQuestion = (course, lessonUuid, questionData) =>
  new Promise((fulfill, reject) => {
    if (course.hasLessonUuid(lessonUuid)) {
      const question = new Question(questionData);
      course.setQuestion(lessonUuid, question)
          .then(() => {
            return fulfill({status: 201, data: {message: 'Ok.', question}});
          })
          .catch((error) => reject(error));
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });

const professorGetAllLessonQuestions = (course, lessonUuid) =>
  new Promise((fulfill, reject) => {
    if (course.hasLessonUuid(lessonUuid)) {
      const {questions} = course.getLessonByUuid(lessonUuid);
      return fulfill({status: 200, data: {message: 'Ok.', questions}});
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });

const professorGetOneLessonQuestion = (course, lessonUuid, questionUuid) =>
  new Promise((fulfill, reject) => {
    course.getQuestion(lessonUuid, questionUuid)
        .then(({question}) => {
          return fulfill({status: 200, data: {message: 'Ok.', question}});
        })
        .catch((error) => reject(error));
  });

const professorUpdateQuestion = (course, lessonUuid, newQuestionData) =>
  new Promise((fulfill, reject) => {
    course.getQuestion(lessonUuid, newQuestionData.uuid)
        .then(() => {
          const question = new Question(newQuestionData);
          course.setQuestion(lessonUuid, question)
              .then(() => {
                return fulfill({
                  status: 201,
                  data: {message: 'Question updated.', question},
                });
              })
              .catch((error) => reject(error));
        })
        .catch((error) => reject(error));
  });

const professorDeleteQuestion = (course, lessonUuid, questionUuid) =>
  new Promise((fulfill, reject) => {
    course.deleteQuestion(lessonUuid, questionUuid)
        .then(() => {
          return fulfill({
            status: 200,
            data: {message: 'Question deleted.'},
          });
        })
        .catch((error) => reject(error));
  });

module.exports = {
  professorRegisterQuestion,
  professorGetAllLessonQuestions,
  professorGetOneLessonQuestion,
  professorUpdateQuestion,
  professorDeleteQuestion,
};
