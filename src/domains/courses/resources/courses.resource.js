'use strict';

const router = require('express').Router(); // eslint-disable-line new-cap
const {body, validationResult} = require('express-validator');

const {
  validateProfessor,
  validateCourseProfessor,
} = require('@middlewares');

const {
  professorRegisterCourse,
  professorUpdateCourse,
  professorDeleteCourse,
  readCourses,
  readCourse,
  enrollCourse,
} = require('../controllers/courses.controller.js');

router.post('/professor', [
  validateProfessor,
  body('area').isString(),
  body('description').isString(),
  body('hours').isInt({min: 1}),
  body('level').isString(),
  body('name').isString(),
  body('previous_courses').isArray(),
  body('previous_courses.*').isString(),
  body('tags').isArray(),
  body('tags.*').isString(),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    professorRegisterCourse(req.body, req.appended.user)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.get('/:courseUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  const course = req.appended.course;
  delete course._id;
  res.status(200).json({message: 'Ok.', course});
});

router.put('/:courseUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
  body('area').isString(),
  body('description').isString(),
  body('hours').isInt({min: 1}),
  body('level').isString(),
  body('name').isString(),
  body('previous_courses').isArray(),
  body('previous_courses.*').isString(),
  body('professors').isArray(),
  body('professors.*').isString(),
  body('tags').isArray(),
  body('tags.*').isString(),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    professorUpdateCourse(req.appended.course, req.body)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.delete('/:courseUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  professorDeleteCourse(req.appended.course)
      .then(() => {
        return res.status(200).json({message: 'Course deleted.'});
      })
      .catch(({status, data}) => res.status(status).json(data));
});

router.get('/', (req, res) => {
  readCourses(req.appended.user)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

router.get('/:courseUuid', (req, res) => {
  readCourse(req.appended.user, req.params.courseUuid)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

router.post('/:courseUuid/enroll', (req, res) => {
  enrollCourse(req.appended.user, req.params.courseUuid)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

module.exports = router;
