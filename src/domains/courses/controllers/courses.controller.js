'use strict';

const {Course, User, Lesson} = require('@models');

const professorRegisterCourse = (course, professor) =>
  new Promise((fulfill, reject) => {
    Course.findByUuids(course.previous_courses)
        .then(({courses}) => {
          if (courses.length === course.previous_courses.length) {
            course.professors = [professor.uuid];
            Course.register(course)
                .then(({course}) => {
                  delete course._id;
                  return fulfill({
                    status: 201,
                    data: {message: 'Course registered.', course},
                  });
                })
                .catch((error) => reject(error));
          } else {
            return reject({
              status: 409,
              // eslint-disable-next-line max-len
              data: {message: 'previous_courses must be an array of valid courses uuids.'},
            });
          }
        })
        .catch((error) => reject(error));
  });

const professorUpdateCourse = (course, newCouseData) =>
  new Promise((fulfill, reject) => {
    const promises = [];
    promises.push(new Promise((fulfill) => {
      Course.findByName(newCouseData.name)
          .then((result) => fulfill(result))
          .catch(() => fulfill({course}));
    }));
    promises.push(Course.findByUuids(newCouseData.previous_courses));
    promises.push(User.findByUuids(newCouseData.professors));
    Promise.all(promises)
        .then(([courseWithSameName, {courses}, {users}]) => {
          courseWithSameName = courseWithSameName.course;
          if (course.getUuid() === courseWithSameName.getUuid()) {
            courses = courses
                .map((course) => course.getUuid());
            users = users
                .filter((user) => user.isProfessor())
                .map((user) => user.getUuid());
            if (courses.length !== newCouseData.previous_courses.length) {
              return reject({
                status: 409,
                // eslint-disable-next-line max-len
                data: {message: 'previous_courses must be an array of valid courses uuids.'},
              });
            }
            if (users.length < 1 ||
                users.length !== newCouseData.professors.length) {
              return reject({
                status: 409,
                // eslint-disable-next-line max-len
                data: {message: 'professors must be an array of valid professors uuids.'},
              });
            }
            course.update(newCouseData);
            course.write()
                .then(() => {
                  delete course._id;
                  return fulfill({
                    status: 200,
                    data: {message: 'Course updated.', course},
                  });
                })
                .catch((error) => reject(error));
          } else {
            return reject({
              status: 409,
              data: {message: 'Course with the same name already exists.'},
            });
          }
        })
        .catch((error) => reject(error));
  });

const professorDeleteCourse = (course) => course.delete();

const readCourses = (user) => new Promise((fulfill, reject) => {
  Course.findAll()
      .then(({courses}) => {
        courses = courses.map((course) => {
          const courseOverviewData = course.getOverviewData();
          courseOverviewData.approved = user.hasApprovedCourse(course);
          courseOverviewData.can_access = courseOverviewData.approved ||
          user.hasApprovedCoursesUuids(course.getPreviousCourses());
          return courseOverviewData;
        });
        return fulfill({
          status: 200,
          data: {message: 'Ok.', courses},
        });
      })
      .catch((error) => reject(error));
});

const readCourse = (user, courseUuid) => new Promise((fulfill, reject) => {
  Course.findByUuid(courseUuid)
      .then(({course}) => {
        const promises = [];
        promises.push(Course.findByUuids(course.previous_courses));
        promises.push(User.findByUuids(course.professors));
        Promise.all(promises)
            .then(([{courses}, {users}]) => {
              const approved = user.hasApprovedCourse(course);
              const canAccess = approved ||
              user.hasApprovedCoursesUuids(course.getPreviousCourses());
              course = course.getDetailedData();
              course.lessons = Object.values(course.lessons)
                  .map((lesson) => (new Lesson(lesson)).getOverviewData());
              course.previous_courses = courses
                  .map((course) => course.getOverviewData());
              course.professors = users
                  .map((user) => user.getOverviewData());
              course.approved = approved;
              course.can_access = canAccess;
              return fulfill({
                status: 200,
                data: {message: 'Ok.', course},
              });
            })
            .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
});

const enrollCourse = (user, courseUuid) =>
  new Promise((fulfill, reject) => {
    Course.findByUuid(courseUuid)
        .then(({course}) => {
          if (user.hasApprovedCoursesUuids(course.getPreviousCourses())) {
            user.enrollCourse(course);
            user.write()
                .then(() => {
                  return fulfill({
                    status: 200,
                    data: {message: 'Ok.'},
                  });
                })
                .catch((error) => reject(error));
          } else {
            return reject({
              status: 409,
              data: {message: 'Previous courses must be approved.'},
            });
          }
        })
        .catch((error) => reject(error));
  });

module.exports = {
  professorRegisterCourse,
  professorUpdateCourse,
  professorDeleteCourse,
  readCourses,
  readCourse,
  enrollCourse,
};
