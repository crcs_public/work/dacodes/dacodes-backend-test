'use strict';

const router = require('express').Router(); // eslint-disable-line new-cap
const {body, validationResult} = require('express-validator');

const {register, login} = require('../controllers/users.controller.js');

router.post('/register', [
  body('email').isEmail(),
  body('password').isString({min: 8}),
  body('type').isIn(['professor', 'student']),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    const {email, password, type} = req.body;
    register(email, password, type)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.post('/login', [
  body('email').isEmail(),
  body('password').isString({min: 8}),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    const {email, password} = req.body;
    login(email, password)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

module.exports = router;
