'use strict';

const User = require('@models/user.model.js');

const register = (email, password, type) => new Promise((fulfill, reject) => {
  User.register(email, password, type)
      .then(() => fulfill({status: 201, data: {message: 'User registered.'}}))
      .catch((error) => reject(error));
});

const login = (email, password) => new Promise((fulfill, reject) => {
  User.findByEmail(email)
      .then(({user}) => user.isPassword({password}))
      .then(({user, isPassword}) => user.setToken({isPassword}))
      .then(({user, isPassword}) => {
        if (isPassword) {
          return fulfill({
            status: 200,
            data: {
              message: 'Ok.',
              token: user.getToken(),
              type: user.getType(),
              uuid: user.getUuid(),
            },
          });
        } else {
          return reject({status: 401, data: {message: 'Wrong password.'}});
        }
      })
      .catch((error) => reject(error));
});

module.exports = {register, login};
