'use strict';

const router = require('express').Router(); // eslint-disable-line new-cap
const {body, validationResult} = require('express-validator');

const {
  professorRegisterLesson,
  professorUpdateLesson,
  professorDeleteLesson,
  readLessons,
  readLesson,
  enrollLesson,
  answerLesson,
} = require('../controllers/lessons.controller.js');

const {
  validateProfessor,
  validateCourseProfessor,
  validateCourseStudent,
} = require('@middlewares');

router.post('/:courseUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
  body('approval_score').isInt({min: 0}),
  body('description').isString(),
  body('name').isString(),
  body('previous_lessons').isArray(),
  body('previous_lessons.*').isString(),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    professorRegisterLesson(req.appended.course, req.body)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.get('/:courseUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  res.status(200).json({message: 'Ok.', lessons: req.appended.course.lessons});
});

router.get('/:courseUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  res.status(200).json({message: 'Ok.', lessons: req.appended.course.lessons});
});

router.get('/:courseUuid/:lessonUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  const lesson = req.appended.course.lessons[req.params.lessonUuid];
  if (typeof lesson !== 'undefined' && lesson !== null) {
    res.status(200).json({message: 'Ok.', lesson});
  } else {
    res.status(404).json({message: 'Lesson not found.'});
  }
});

router.put('/:courseUuid/:lessonUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
  body('approval_score').isInt({min: 0}),
  body('description').isString(),
  body('name').isString(),
  body('previous_lessons').isArray(),
  body('previous_lessons.*').isString(),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    req.body.uuid = req.params.lessonUuid;
    professorUpdateLesson(req.appended.course, req.body)
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

router.delete('/:courseUuid/:lessonUuid/professor', [
  validateProfessor,
  validateCourseProfessor,
], (req, res) => {
  professorDeleteLesson(req.appended.course, req.params.lessonUuid)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

router.get('/:courseUuid', validateCourseStudent, (req, res) => {
  readLessons(req.appended.user, req.appended.course)
      .then(({status, data}) => res.status(status).json(data))
      .catch(({status, data}) => res.status(status).json(data));
});

router.get('/:courseUuid/:lessonUuid',
    validateCourseStudent, (req, res) => {
      readLesson(req.appended.user, req.appended.course, req.params.lessonUuid)
          .then(({status, data}) => res.status(status).json(data))
          .catch(({status, data}) => res.status(status).json(data));
    });

router.get('/:courseUuid/:lessonUuid/enroll',
    validateCourseStudent, (req, res) => {
      enrollLesson(
          req.appended.user,
          req.appended.course,
          req.params.lessonUuid,
      )
          .then(({status, data}) => res.status(status).json(data))
          .catch(({status, data}) => res.status(status).json(data));
    });

router.post('/:courseUuid/:lessonUuid/enroll', [
  validateCourseStudent,
  body('*.answers').isArray(),
], (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    answerLesson(
        req.appended.user,
        req.appended.course,
        req.params.lessonUuid,
        req.body,
    )
        .then(({status, data}) => res.status(status).json(data))
        .catch(({status, data}) => res.status(status).json(data));
  } else {
    return res.status(400).json({errors: errors.array()});
  }
});

module.exports = router;
