'use strict';

const {Lesson, Question} = require('@models');

const professorRegisterLesson = (course, lesson) =>
  new Promise((fulfill, reject) => {
    lesson = new Lesson(lesson);
    const previousLessons = lesson.previous_lessons
        .filter((uuid) => course.hasLessonUuid(uuid));
    if (previousLessons.length === lesson.previous_lessons.length) {
      if (!(course.hasLessonUuid(lesson.getUuid()) ||
            course.hasLessonName(lesson.getName()))) {
        course.setLesson(lesson);
        course.write()
            .then(() => fulfill({
              status: 201,
              data: {message: 'Lesson registered.', lesson},
            }))
            .catch((error) => reject(error));
      } else {
        return reject({
          status: 409,
          data: {message: 'Lesson with the same name already exists.'},
        });
      }
    } else {
      return reject({
        status: 409,
        // eslint-disable-next-line max-len
        data: {message: 'previous_lessons must be an array of valid lessons uuids.'},
      });
    }
  });

const professorUpdateLesson = (course, newLessonData) =>
  new Promise((fulfill, reject) => {
    if (course.hasLessonUuid(newLessonData.uuid)) {
      const previousLessons = newLessonData.previous_lessons
          .filter((uuid) => course.hasLessonUuid(uuid))
          .filter((uuid) => newLessonData.uuid !== uuid);
      if (previousLessons.length === newLessonData.previous_lessons.length) {
        const lesson = new Lesson(newLessonData);
        const previousLesson =
        new Lesson(course.getLessonByUuid(lesson.getUuid()));
        if (lesson.getName() !== previousLesson.getName() &&
            course.hasLessonName(lesson.getName())) {
          return reject({
            status: 409,
            data: {message: 'Lesson with the same name already exists.'},
          });
        } else {
          course.setLesson(lesson);
          course.write()
              .then(() => fulfill({
                status: 201,
                data: {message: 'Lesson updated.', lesson},
              }))
              .catch((error) => reject(error));
        }
      } else {
        return reject({
          status: 409,
          // eslint-disable-next-line max-len
          data: {message: 'previous_lessons must be an array of valid lessons uuids.'},
        });
      }
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });

const professorDeleteLesson = (course, lessonUuid) =>
  new Promise((fulfill, reject) => {
    if (course.hasLessonUuid(lessonUuid)) {
      course.deleteLessonByUuid(lessonUuid);
      course.write()
          .then(() => fulfill({
            status: 201,
            data: {message: 'Lesson deleted.'},
          }))
          .catch((error) => reject(error));
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });

const readLessons = (user, course) =>
  new Promise((fulfill) => {
    const lessons = Object.values(course.getLessons())
        .map((lesson) => {
          lesson = new Lesson(lesson);
          const lessonOverviewData = lesson.getOverviewData();
          lessonOverviewData.approved = user.hasApprovedLesson(course, lesson);
          lessonOverviewData.can_access = lessonOverviewData.approved ||
          user.hasApprovedLessonsUuids(course, lesson.getPreviousLessons());
          return lessonOverviewData;
        });
    return fulfill({
      status: 200,
      data: {message: 'Ok.', lessons},
    });
  });

const readLesson = (user, course, lessonUuid) =>
  new Promise((fulfill) => {
    if (course.hasLessonUuid(lessonUuid)) {
      const lesson = new Lesson(course.getLessonByUuid(lessonUuid));
      const lessonDetailedData = lesson.getDetailedData();
      lessonDetailedData.previous_lessons = lessonDetailedData.previous_lessons
          .filter((lessonUuid) => course.hasLessonUuid(lessonUuid))
          .map((lessonUuid) =>
            (new Lesson(course.getLessonByUuid(lessonUuid))).getOverviewData());
      lessonDetailedData.questions =
      Object.keys(lessonDetailedData.questions).length;
      lessonDetailedData.approved = user.hasApprovedLesson(course, lesson);
      lessonDetailedData.can_access = lessonDetailedData.approved ||
      user.hasApprovedLessonsUuids(course, lesson.getPreviousLessons());
      return fulfill({
        status: 200,
        data: {message: 'Ok.', lesson: lessonDetailedData},
      });
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });

const enrollLesson = (user, course, lessonUuid) =>
  new Promise((fulfill, reject) => {
    if (course.hasLessonUuid(lessonUuid)) {
      let lesson = new Lesson(course.getLessonByUuid(lessonUuid));
      if (user.hasApprovedLessonsUuids(course, lesson.getPreviousLessons())) {
        user.enrollLesson(course, lesson);
        user.write()
            .then(() => {
              lesson = lesson.getDetailedData();
              lesson.previous_lessons = lesson.previous_lessons
                  .filter((lessonUuid) => course.hasLessonUuid(lessonUuid))
                  .map((lessonUuid) =>
                    (new Lesson(course.getLessonByUuid(lessonUuid)))
                        .getOverviewData());
              lesson.questions = Object.values(lesson.questions)
                  .map((question) => (new Question(question))
                      .getOverviewData());
              return fulfill({
                status: 200,
                data: {message: 'Ok.', lesson},
              });
            })
            .catch((error) => reject(error));
      } else {
        return reject({
          status: 409,
          data: {message: 'Previous lessons must be approved.'},
        });
      }
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });

const answerLesson = (user, course, lessonUuid, userAnswers) =>
  new Promise((fulfill, reject) => {
    if (course.hasLessonUuid(lessonUuid)) {
      const lesson = new Lesson(course.getLessonByUuid(lessonUuid));
      const userAnswersData = Object.values(lesson.getQuestions())
          .reduce((userAnswersData, question) => {
            userAnswersData.total += 1;
            question = new Question(question);
            const userAnswer = userAnswers[question.getUuid()];
            if (typeof userAnswer !== 'undefined' && userAnswer !== null) {
              const {valid, correct, score, answers} =
              question.scoreAnswers(userAnswer.answers);
              userAnswersData.answered += 1;
              userAnswersData.valid += valid ? 1 : 0;
              userAnswersData.correct += correct ? 1 : 0;
              userAnswersData.score += score;
              userAnswersData.answers[question.getUuid()] = {
                answers,
                timestamp: new Date(),
              };
            }
            return userAnswersData;
          }, {total: 0, answered: 0, valid: 0,
            correct: 0, score: 0, answers: {}});
      if (userAnswersData.total === userAnswersData.answered) {
        if (userAnswersData.total === userAnswersData.valid) {
          user.setLessonResults(course, lesson, userAnswersData);
          user.write()
              .then(() => {
                return fulfill({status: 200, data: {message: 'Ok.'}});
              })
              .catch((error) => reject(error));
        } else {
          return reject({
            status: 400,
            data: {message: 'There is an invalid answer.'},
          });
        }
      } else {
        return reject({
          status: 400,
          data: {message: 'All questions must be answered.'},
        });
      }
    } else {
      return reject({status: 404, data: {message: 'Lesson not found.'}});
    }
  });


module.exports = {
  professorRegisterLesson,
  professorUpdateLesson,
  professorDeleteLesson,
  readLessons,
  readLesson,
  enrollLesson,
  answerLesson,
};
