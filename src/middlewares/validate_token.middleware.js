'use strict';

const User = require('@models/user.model.js');

const validateToken = (req, res, next) => {
  let token = req.get('Authorization');
  if (typeof token === 'string' && /Bearer\s/.test(token)) {
    token = token.replace('Bearer ', '');
    User.findByToken(token)
        .then(({user}) => {
          const {timestamp} = user.token;
          const ageHours = (new Date - timestamp) / 36e5;
          if (ageHours < process.env.JWT_EXPIRATION_HOURS) {
            if (typeof req.appended === 'undefined' || req.appended === null) {
              req.appended = {};
            }
            req.appended.user = user;
            user.updateToken().then(() => {}).catch(() => {});
            return next();
          } else {
            res.status(401).json({message: 'Expired Bearer Token.'});
          }
        })
        .catch(() => {
          res.status(401).json({message: 'Unauthorized Bearer Token.'});
        });
  } else {
    res.status(400).json({message: 'Missing Bearer Token.'});
  }
};

module.exports = validateToken;
