'use strict';

const Course = require('@models/course.model.js');

const validateCourseProfessor = (req, res, next) => {
  if (typeof req.params.courseUuid === 'string') {
    Course.findByUuid(req.params.courseUuid)
        .then(({course}) => {
          const professorUuid = req.appended.user.uuid;
          if (course.hasProfessor(professorUuid)) {
            if (typeof req.appended === 'undefined' || req.appended === null) {
              req.appended = {};
            }
            req.appended.course = course;
            return next();
          } else {
            return res.status(401)
                // eslint-disable-next-line max-len
                .json({message: 'Only course professors can perform this action.'});
          }
        })
        .catch(({status, data}) => {
          return res.status(status).json(data);
        });
  } else {
    return res.status(400)
        .json({message: 'params must have uuid.'});
  }
};

module.exports = validateCourseProfessor;
