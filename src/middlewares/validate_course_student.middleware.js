'use strict';

const Course = require('@models/course.model.js');

const validateCourseStudent = (req, res, next) => {
  if (typeof req.params.courseUuid === 'string') {
    Course.findByUuid(req.params.courseUuid)
        .then(({course}) => {
          const {user} = req.appended;
          if (user.isEnrolledInCourse(course)) {
            if (typeof req.appended === 'undefined' || req.appended === null) {
              req.appended = {};
            }
            req.appended.course = course;
            return next();
          } else {
            return res.status(401)
                // eslint-disable-next-line max-len
                .json({message: 'Only enrolled students can perform this action.'});
          }
        })
        .catch(({status, data}) => {
          return res.status(status).json(data);
        });
  } else {
    return res.status(400)
        .json({message: 'params must have uuid.'});
  }
};

module.exports = validateCourseStudent;
