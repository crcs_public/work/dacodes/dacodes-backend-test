'use strict';

const validateProfessor = (req, res, next) => {
  if (typeof req.appended !== 'undefined' && req.appended !== null &&
      typeof req.appended.user !== 'undefined' && req.appended.user !== null &&
      req.appended.user.isProfessor() === true) {
    return next();
  } else {
    res.status(401).json({message: 'Only professors can perform this action.'});
  }
};

module.exports = validateProfessor;
