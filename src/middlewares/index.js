'use strict';

const validateToken = require('./validate_token.middleware.js');
const validateProfessor = require('./validate_professor.middleware.js');
const validateCourseProfessor =
require('./validate_course_professor.middleware.js');
const validateCourseStudent =
require('./validate_course_student.middleware.js');

module.exports = {
  validateToken,
  validateProfessor,
  validateCourseProfessor,
  validateCourseStudent,
};
