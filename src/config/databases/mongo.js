'use strict';

const MongoClient = require('mongodb').MongoClient;

/**
 * Mongo
 */
class Mongo {
  /**
   * [constructor description]
   */
  constructor() {
    this.host = process.env.DATABASE_MONGODB_HOST;
    this.port = process.env.DATABASE_MONGODB_PORT;
    this.user = process.env.DATABASE_MONGODB_USER;
    this.password = process.env.DATABASE_MONGODB_PASSWORD;
    this.database = process.env.DATABASE_MONGODB_DATABASE;
  }
  /**
   * [getUrl description]
   * @return {[type]} [description]
   */
  getUrl() {
    let url = 'mongodb://';
    if (this.user.length > 0 && this.password.length > 0) {
      url += this.user + ':' + this.password + '@';
    }
    url += this.host + ':' + this.port;
    return url;
  }
  /**
   * [getDatabase description]
   * @return {[type]} [description]
   */
  getDatabase() {
    return this.database;
  }
  /**
   * [queryPromise description]
   * @param  {[type]} promise    [description]
   * @param  {[type]} parameters [description]
   * @return {[type]}            [description]
   */
  queryPromise(promise, parameters) {
    return new Promise((fulfill, reject) => {
      MongoClient.connect(this.getUrl(), (err, client) => {
        if (err) {
          return reject({
            status: 500,
            data: {message: 'Could not connect to the database.'},
          });
        } else {
          const db = client.db(this.getDatabase());
          promise(db, parameters)
              .then((result) => {
                client.close();
                return fulfill(result);
              })
              .catch((error) => {
                client.close();
                return reject(error);
              });
        }
      });
    });
  }
}

module.exports = Mongo;

