'use strict';

const {mongo} = require('@databases');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {v4: uuidv4} = require('uuid');

/**
 * User
 */
class User {
  /**
   * [constructor description]
   * @param  {[type]} data [description]
   */
  constructor(data) {
    this._id = data._id;
    this.uuid = data.uuid;
    this.email = data.email;
    this.password = data.password;
    this.token = data.token;
    this.type = data.type;
    this.courses = typeof data.courses !== 'undefined' &&
    data.courses !== null ?
    data.courses : {};
  }
  /**
   * [getType description]
   * @return {[type]} [description]
   */
  getType() {
    return this.type;
  }
  /**
   * [getUuid description]
   * @return {[type]} [description]
   */
  getUuid() {
    return this.uuid;
  }
  /**
   * [getOverviewData description]
   * @return {[type]} [description]
   */
  getOverviewData() {
    return {
      email: this.email,
      uuid: this.uuid,
    };
  }
  /**
   * [isPassword description]
   * @param  {[type]}  runway [description]
   * @return {Boolean}        [description]
   */
  isPassword(runway) {
    return new Promise((fulfill, reject) => {
      bcrypt.compare(runway.password, this.password)
          .then((isPassword) => fulfill({
            ...runway, user: this, isPassword}))
          .catch(() => reject({
            status: 500,
            data: {message: 'Internal server error.'},
          }));
    });
  }
  /**
   * [isProfessor description]
   * @return {Boolean} [description]
   */
  isProfessor() {
    return this.type === 'professor';
  }
  /**
   * [setToken description]
   * @param  {[type]} runway [description]
   * @return {[type]}        [description]
   */
  setToken(runway) {
    const token = jwt.sign(
        {fingerprint: uuidv4()}, // Any random data can be inserted into the jwt
        process.env.JWT_PRIVATE_KEY);
    const timestamp = new Date();
    this.token = {jwt: token, timestamp};
    return this.write(runway);
  }
  /**
   * [updateToken description]
   * @param  {[type]} runway [description]
   * @return {[type]}        [description]
   */
  updateToken(runway) {
    this.token.timestamp = new Date();
    return this.write(runway);
  }
  /**
   * [getToken description]
   * @return {[type]} [description]
   */
  getToken() {
    return this.token.jwt;
  }
  /**
   * [write description]
   * @param  {[type]} runway [description]
   * @return {[type]}        [description]
   */
  write(runway) {
    const promise = (db, {runway}) => new Promise((fulfill, reject) => {
      db.collection('user')
          .updateOne({_id: this._id}, {$set: this}, (err, result)=> {
            if (err) {
              return reject({
                status: 500,
                data: {message: 'Database fail.',
                }});
            } else {
              return fulfill({...runway, user: this});
            }
          });
    });
    return mongo.queryPromise(promise, {runway});
  }
  /**
   * [hasApprovedCourse description]
   * @param  {[type]}  course [description]
   * @return {Boolean}        [description]
   */
  hasApprovedCourse(course) {
    return this.hasApprovedCoursesUuids([course.getUuid()]);
  }
  /**
   * [hasApprovedCoursesUuids description]
   * @param  {[type]}  coursesUuids [description]
   * @return {Boolean}              [description]
   */
  hasApprovedCoursesUuids(coursesUuids) {
    return coursesUuids.reduce((flag, courseUuid) => {
      return flag &&
      courseUuid in this.courses &&
      this.courses[courseUuid].approved;
    }, true);
  }
  /**
   * [enrollCourse description]
   * @param  {[type]} course [description]
   */
  enrollCourse(course) {
    const courseUuid = course.getUuid();
    if (!(courseUuid in this.courses)) {
      this.courses[courseUuid] = {
        approved: false,
        approved_timestamp: null,
        begin_timestamp: new Date(),
        uuid: courseUuid,
      };
    }
  }
  /**
   * [isEnrolledInCourse description]
   * @param  {[type]}  course [description]
   * @return {Boolean}        [description]
   */
  isEnrolledInCourse(course) {
    return course.getUuid() in this.courses;
  }
  /**
   * [setCourseApproval description]
   * @param {[type]} course [description]
   * @param {[type]} flag   [description]
   */
  setCourseApproval(course, flag) {
    if (this.isEnrolledInCourse(course)) {
      this.courses[course.getUuid()].approved = flag;
      if (flag) {
        this.courses[course.getUuid()].approved_timestamp = new Date();
      }
    }
  }
  /**
   * [updateCourseApproval description]
   * @param  {[type]} course [description]
   */
  updateCourseApproval(course) {
    if (this.isEnrolledInCourse(course) &&
        !this.hasApprovedCourse(course)) {
      const lessonsUuids = Object.keys(course.getLessons());
      this.setCourseApproval(course,
          this.hasApprovedLessonsUuids(course, lessonsUuids));
    }
  }
  /**
   * [hasApprovedLesson description]
   * @param  {[type]}  course [description]
   * @param  {[type]}  lesson [description]
   * @return {Boolean}        [description]
   */
  hasApprovedLesson(course, lesson) {
    return this.hasApprovedLessonsUuids(course, [lesson.getUuid()]);
  }
  /**
   * [hasApprovedLessonsUuids description]
   * @param  {[type]}  course       [description]
   * @param  {[type]}  lessonsUuids [description]
   * @return {Boolean}              [description]
   */
  hasApprovedLessonsUuids(course, lessonsUuids) {
    if (this.isEnrolledInCourse(course)) {
      let {lessons} = this.courses[course.getUuid()];
      if (typeof lessons === 'undefined' || lessons === null) {
        lessons = {};
      }
      return lessonsUuids.reduce((flag, lessonUuid) => {
        return flag &&
        lessonUuid in lessons &&
        lessons[lessonUuid].approved;
      }, true);
    }
    return false;
  }
  /**
   * [enrollLesson description]
   * @param  {[type]} course [description]
   * @param  {[type]} lesson [description]
   */
  enrollLesson(course, lesson) {
    const courseUuid = course.getUuid();
    const lessonUuid = lesson.getUuid();
    if (typeof this.courses[courseUuid] !== 'undefined' &&
        this.courses[courseUuid] !== null) {
      if (typeof this.courses[courseUuid].lessons === 'undefined' ||
          this.courses[courseUuid].lessons === null) {
        this.courses[courseUuid].lessons = {};
      }
      if (!(lessonUuid in this.courses[courseUuid].lessons)) {
        this.courses[courseUuid].lessons[lessonUuid] = {
          approved: false,
          approved_timestamp: null,
          begin_timestamp: new Date(),
          uuid: lessonUuid,
        };
      }
    }
  }
  /**
   * [isEnrolledInLessson description]
   * @param  {[type]}  course [description]
   * @param  {[type]}  lesson [description]
   * @return {Boolean}        [description]
   */
  isEnrolledInLessson(course, lesson) {
    return this.isEnrolledInCourse(course) &&
    lesson.getUuid() in this.courses[course.getUuid()].lessons;
  }
  /**
   * [setLessonResults description]
   * @param {[type]} course        [description]
   * @param {[type]} lesson        [description]
   * @param {[type]} lessonResults [description]
   */
  setLessonResults(course, lesson, lessonResults) {
    if (this.isEnrolledInLessson(course, lesson) &&
        !this.hasApprovedLesson(course, lesson)) {
      this.courses[course.getUuid()]
          .lessons[lesson.getUuid()].questions = lessonResults.answers;
      if (lessonResults.score >= lesson.getApprovalScore()) {
        this.courses[course.getUuid()]
            .lessons[lesson.getUuid()].approved = true;
        this.courses[course.getUuid()]
            .lessons[lesson.getUuid()].approved_timestamp = new Date();
        this.updateCourseApproval(course);
      }
    }
  }
  /**
   * [findByEmail description]
   * @param  {[type]} email [description]
   * @return {[type]}       [description]
   */
  static findByEmail(email) {
    const promise = (db, {email}) =>
      new Promise((fulfill, reject) => {
        db.collection('user').findOne({email}, (err, result) => {
          if (err) {
            return reject({status: 500, data: {message: 'Database fail.'}});
          } else {
            if (typeof result !== 'undefined' && result !== null) {
              return fulfill({user: new this(result)});
            } else {
              return reject({status: 404, data: {message: 'User not found.'}});
            }
          }
        });
      });
    return mongo.queryPromise(promise, {email});
  }
  /**
   * [findByUuids description]
   * @param  {[type]} uuids [description]
   * @return {[type]}       [description]
   */
  static findByUuids(uuids) {
    const promise = (db, {uuids}) =>
      new Promise((fulfill, reject) => {
        db.collection('user').find({uuid: {$in: uuids}})
            .toArray((err, docs) => {
              if (err) {
                return reject({status: 500, data: {message: 'Database fail.'}});
              } else {
                const users = docs.map((doc) => new this(doc));
                return fulfill({users});
              }
            });
      });
    return mongo.queryPromise(promise, {uuids});
  }
  /**
   * [findByToken description]
   * @param  {[type]} token [description]
   * @return {[type]}       [description]
   */
  static findByToken(token) {
    const promise = (db, {token}) =>
      new Promise((fulfill, reject) => {
        db.collection('user').findOne({'token.jwt': token}, (err, result) => {
          if (err) {
            return reject({status: 500, data: {message: 'Database fail.'}});
          } else {
            if (typeof result !== 'undefined' && result !== null) {
              return fulfill({user: new this(result)});
            } else {
              return reject({status: 404, data: {message: 'User not found.'}});
            }
          }
        });
      });
    return mongo.queryPromise(promise, {token});
  }
  /**
   * [register description]
   * @param  {[type]} email    [description]
   * @param  {[type]} password [description]
   * @param  {[type]} type     [description]
   * @return {[type]}          [description]
   */
  static register(email, password, type) {
    return new Promise((fulfill, reject) => {
      this.findByEmail(email)
          .then(({user}) => {
            return reject({
              status: 409,
              data: {message: 'User already exists.'},
            });
          })
          .catch(({status, data}) => {
            if (status !== 404) {
              return reject({status, data});
            } else {
              const promise = (db, {email, password}) =>
                new Promise((fulfill, reject) => {
                  bcrypt.hash(password, 10, (err, hash) => {
                    db.collection('user')
                        .insertOne({
                          email,
                          password: hash,
                          type,
                          uuid: uuidv4(),
                        }, (err, result) => {
                          if (err) {
                            return reject({
                              status: 500,
                              data: {message: 'Database fail.',
                              }});
                          } else {
                            return fulfill({user: new this({
                              _id: result.insertedId,
                              email,
                              password,
                            })});
                          }
                        });
                  });
                });
              mongo.queryPromise(promise, {email, password})
                  .then((result) => fulfill(result))
                  .catch((error) => reject(error));
            }
          });
    });
  }
}

module.exports = User;
