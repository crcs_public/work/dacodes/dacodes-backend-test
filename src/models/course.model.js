'use strict';

const {mongo} = require('@databases');
const {v4: uuidv4} = require('uuid');

/**
 * Course
 */
class Course {
  /**
   * [constructor description]
   * @param  {[type]} data [description]
   */
  constructor(data) {
    this._id = data._id;
    this.area = data.area;
    this.description = data.description;
    this.hours = data.hours;
    this.lessons = typeof data.lessons === 'object' ? data.lessons : {};
    this.level = data.level;
    this.name = data.name;
    this.previous_courses = data.previous_courses;
    this.professors = data.professors;
    this.tags = data.tags;
    this.uuid = data.uuid;
  }
  /**
   * [getLessons description]
   * @return {[type]} [description]
   */
  getLessons() {
    return this.lessons;
  }
  /**
   * [getPreviousCourses description]
   * @return {[type]} [description]
   */
  getPreviousCourses() {
    return this.previous_courses;
  }
  /**
   * [getUuid description]
   * @return {[type]} [description]
   */
  getUuid() {
    return this.uuid;
  }
  /**
   * [update description]
   * @param  {[type]} data [description]
   */
  update(data) {
    for (const key of Object.keys(this)) {
      if (typeof data[key] !== 'undefined' && data[key] !== null) {
        this[key] = data[key];
      }
    }
  }
  /**
   * [overviewData description]
   * @return {[type]} [description]
   */
  getOverviewData() {
    return {
      area: this.area,
      description: this.description,
      hours: this.hours,
      level: this.level,
      name: this.name,
      tags: this.tags,
      uuid: this.uuid,
    };
  }
  /**
   * [getDetailedData description]
   * @return {[type]} [description]
   */
  getDetailedData() {
    const overviewData = this.getOverviewData();
    return {
      ...overviewData,
      lessons: this.lessons,
      previous_courses: this.previous_courses,
      professors: this.professors,
    };
  }
  /**
   * [write description]
   * @param  {[type]} runway [description]
   * @return {[type]}        [description]
   */
  write(runway) {
    const promise = (db, {runway}) => new Promise((fulfill, reject) => {
      db.collection('course')
          .updateOne({_id: this._id}, {$set: this}, (err, result)=> {
            if (err) {
              return reject({
                status: 500,
                data: {message: 'Database fail.',
                }});
            } else {
              return fulfill({...runway, course: this});
            }
          });
    });
    return mongo.queryPromise(promise, {runway});
  }
  /**
   * [hasProfessor description]
   * @param  {[type]}  professorUuid [description]
   * @return {Boolean}               [description]
   */
  hasProfessor(professorUuid) {
    return this.professors.indexOf(professorUuid) !== -1;
  }
  /**
   * [delete description]
   * @return {[type]} [description]
   */
  delete() {
    const promise = (db) =>
      new Promise((fulfill, reject) => {
        db.collection('course')
            .deleteOne({uuid: this.getUuid()}, (err, result) => {
              if (err) {
                return reject({status: 500, data: {message: 'Database fail.'}});
              } else {
                if (result.deletedCount > 0) {
                  return fulfill();
                } else {
                  return reject({
                    status: 404,
                    data: {message: 'Course not found.'},
                  });
                }
              }
            });
      });
    return mongo.queryPromise(promise);
  }
  /**
   * [hasLessonUuid description]
   * @param  {[type]}  uuid [description]
   * @return {Boolean}      [description]
   */
  hasLessonUuid(uuid) {
    return uuid in this.lessons;
  }
  /**
   * [getLessonByUuid description]
   * @param  {[type]} uuid [description]
   * @return {[type]}      [description]
   */
  getLessonByUuid(uuid) {
    return this.lessons[uuid];
  }
  /**
   * [deleteLessonByUuid description]
   * @param  {[type]} uuid [description]
   */
  deleteLessonByUuid(uuid) {
    delete this.lessons[uuid];
  }
  /**
   * [hasLessonName description]
   * @param  {[type]}  name [description]
   * @return {Boolean}      [description]
   */
  hasLessonName(name) {
    return Object.values(this.lessons)
        .filter((lesson) => lesson.name === name).length > 0;
  }
  /**
   * [setLesson description]
   * @param {[type]} lesson [description]
   */
  setLesson(lesson) {
    this.lessons[lesson.getUuid()] = lesson;
  }
  /**
   * [setQuestion description]
   * @param  {[type]} lessonUuid [description]
   * @param  {[type]} question   [description]
   * @return {[type]}            [description]
   */
  setQuestion(lessonUuid, question) {
    return new Promise((fulfill, reject) => {
      if (this.hasLessonUuid(lessonUuid)) {
        const promise = (db, {lessonUuid, question}) =>
          new Promise((fulfill, reject) => {
            const key = 'lessons.' + lessonUuid +
            '.questions.' + question.getUuid();
            db.collection('course')
                .updateOne({_id: this._id},
                    {$set: {[key]: question}}, (err, result)=> {
                      if (err) {
                        return reject({
                          status: 500,
                          data: {message: 'Database fail.',
                          }});
                      } else {
                        return fulfill();
                      }
                    });
          });
        mongo.queryPromise(promise, {lessonUuid, question})
            .then(() => fulfill())
            .catch((error) => reject(error));
      } else {
        return reject({status: 404, data: {message: 'Lesson not found.'}});
      }
    });
  }
  /**
   * [getQuestion description]
   * @param  {[type]} lessonUuid   [description]
   * @param  {[type]} questionUuid [description]
   * @return {[type]}              [description]
   */
  getQuestion(lessonUuid, questionUuid) {
    return new Promise((fulfill, reject) => {
      if (this.hasLessonUuid(lessonUuid)) {
        const {questions} = this.getLessonByUuid(lessonUuid);
        if (questionUuid in questions) {
          const question = questions[questionUuid];
          return fulfill({question});
        } else {
          return reject({status: 404, data: {message: 'Question not found.'}});
        }
      } else {
        return reject({status: 404, data: {message: 'Lesson not found.'}});
      }
    });
  }
  /**
   * [deleteQuestion description]
   * @param  {[type]} lessonUuid   [description]
   * @param  {[type]} questionUuid [description]
   * @return {[type]}              [description]
   */
  deleteQuestion(lessonUuid, questionUuid) {
    return new Promise((fulfill, reject) => {
      this.getQuestion(lessonUuid, questionUuid)
          .then(({question}) => {
            const promise = (db, {lessonUuid, question}) =>
              new Promise((fulfill, reject) => {
                const key = 'lessons.' + lessonUuid +
                '.questions.' + question.uuid;
                db.collection('course')
                    .updateOne({_id: this._id},
                        {$unset: {[key]: question}}, (err, result)=> {
                          if (err) {
                            return reject({
                              status: 500,
                              data: {message: 'Database fail.',
                              }});
                          } else {
                            return fulfill();
                          }
                        });
              });
            mongo.queryPromise(promise, {lessonUuid, question})
                .then(() => fulfill())
                .catch((error) => reject(error));
          })
          .catch((error) => reject(error));
    });
  }
  /**
   * [findByName description]
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  static findByName(name) {
    const promise = (db, {name}) =>
      new Promise((fulfill, reject) => {
        db.collection('course').findOne({name}, (err, result) => {
          if (err) {
            return reject({status: 500, data: {message: 'Database fail.'}});
          } else {
            if (typeof result !== 'undefined' && result !== null) {
              return fulfill({course: new this(result)});
            } else {
              return reject({
                status: 404,
                data: {message: 'Course not found.'},
              });
            }
          }
        });
      });
    return mongo.queryPromise(promise, {name});
  }
  /**
   * [findByUuid description]
   * @param  {[type]} uuid [description]
   * @return {[type]}      [description]
   */
  static findByUuid(uuid) {
    const promise = (db, {uuid}) =>
      new Promise((fulfill, reject) => {
        db.collection('course').findOne({uuid}, (err, result) => {
          if (err) {
            return reject({status: 500, data: {message: 'Database fail.'}});
          } else {
            if (typeof result !== 'undefined' && result !== null) {
              return fulfill({course: new this(result)});
            } else {
              return reject({
                status: 404,
                data: {message: 'Course not found.'},
              });
            }
          }
        });
      });
    return mongo.queryPromise(promise, {uuid});
  }
  /**
   * [findByUuids description]
   * @param  {[type]} uuids [description]
   * @return {[type]}       [description]
   */
  static findByUuids(uuids) {
    const promise = (db, {uuids}) =>
      new Promise((fulfill, reject) => {
        db.collection('course').find({uuid: {$in: uuids}})
            .toArray((err, docs) => {
              if (err) {
                return reject({status: 500, data: {message: 'Database fail.'}});
              } else {
                const courses = docs.map((doc) => new this(doc));
                return fulfill({courses});
              }
            });
      });
    return mongo.queryPromise(promise, {uuids});
  }
  /**
   * [findAll description]
   * @return {[type]} [description]
   */
  static findAll() {
    const promise = (db) =>
      new Promise((fulfill, reject) => {
        db.collection('course').find({})
            .toArray((err, docs) => {
              if (err) {
                return reject({status: 500, data: {message: 'Database fail.'}});
              } else {
                const courses = docs.map((doc) => new this(doc));
                return fulfill({courses});
              }
            });
      });
    return mongo.queryPromise(promise);
  }
  /**
   * [register description]
   * @param  {[type]} course [description]
   * @return {[type]}        [description]
   */
  static register(course) {
    return new Promise((fulfill, reject) => {
      this.findByName(course.name)
          .then(({course}) => {
            return reject({
              status: 409,
              data: {message: 'Course with the same name already exists.'},
            });
          })
          .catch(({status, data}) => {
            if (status !== 404) {
              return reject({status, data});
            } else {
              const promise = (db, {data}) =>
                new Promise((fulfill, reject) => {
                  course.uuid = uuidv4();
                  db.collection('course')
                      .insertOne(course, (err, result) => {
                        if (err) {
                          return reject({
                            status: 500,
                            data: {message: 'Database fail.',
                            }});
                        } else {
                          return fulfill({course: new this({
                            ...course,
                            _id: result.insertedId,
                            lessons: {},
                          })});
                        }
                      });
                });
              mongo.queryPromise(promise, {data})
                  .then((result) => fulfill(result))
                  .catch((error) => reject(error));
            }
          });
    });
  }
}

module.exports = Course;
