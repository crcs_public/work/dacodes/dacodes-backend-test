'use strict';

const {v4: uuidv4} = require('uuid');

/**
 * Question
 */
class Question {
  /**
   * [constructor description]
   * @param  {[type]} data [description]
   */
  constructor(data) {
    this.answers = data.answers;
    this.options = data.options;
    this.score = data.score;
    this.text = data.text;
    this.type = data.type;
    this.uuid = typeof data.uuid !== 'undefined' && data.uuid !== null ?
    data.uuid : uuidv4();
  }
  /**
   * [getUuid description]
   * @return {[type]} [description]
   */
  getUuid() {
    return this.uuid;
  }
  /**
   * [getOverviewData description]
   * @return {[type]} [description]
   */
  getOverviewData() {
    return {
      options: this.type === 'boolean' ? [false, true] : this.options,
      score: this.score,
      text: this.text,
      type: this.type,
      uuid: this.uuid,
    };
  }
  /**
   * [scoreAnswers description]
   * @param  {[type]} answers [description]
   * @return {[type]}         [description]
   */
  scoreAnswers(answers) {
    let valid = false;
    let correct = false;
    let score = 0;
    if (this.type === 'boolean') {
      valid = answers.length === 1 && typeof answers[0] === 'boolean';
      correct = this.answers.indexOf(answers[0]) !== -1;
      score = valid && correct ? this.score : 0;
    } else if (this.type === 'multiple-all-answer') {
      valid = answers.length >= 1;
      correct = this.answers.length === answers.length &&
      this.answers.reduce((flag, answer) => flag &&
        answers.indexOf(answer) !== -1, true);
      score = valid && correct ? this.score : 0;
    } else if (this.type === 'multiple-one-answer') {
      valid = answers.length === 1;
      correct = this.answers.indexOf(answers[0]) !== -1;
      score = valid && correct ? this.score : 0;
    } else if (this.type === 'one-answer') {
      valid = answers.length === 1;
      correct = this.answers.indexOf(answers[0]) !== -1;
      score = valid && correct ? this.score : 0;
    } else {
      console.log('WARNING: unknown question type.');
      answers = [];
    }
    return {valid, correct, score, answers};
  }
}

module.exports = Question;
