'use strict';

module.exports = {
  Course: require('./course.model.js'),
  Lesson: require('./lesson.model.js'),
  Question: require('./question.model.js'),
  User: require('./user.model.js'),
};
