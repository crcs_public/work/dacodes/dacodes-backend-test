'use strict';

const {v4: uuidv4} = require('uuid');

/**
 * Lesson
 */
class Lesson {
  /**
   * [constructor description]
   * @param  {[type]} data [description]
   */
  constructor(data) {
    this.approval_score = data.approval_score;
    this.description = data.description;
    this.name = data.name;
    this.previous_lessons = data.previous_lessons;
    this.questions = typeof data.questions === 'object' ? data.questions : {};
    this.uuid = typeof data.uuid !== 'undefined' && data.uuid !== null ?
    data.uuid : uuidv4();
  }
  /**
   * [getApprovalScore description]
   * @return {[type]} [description]
   */
  getApprovalScore() {
    return this.approval_score;
  }
  /**
   * [getName description]
   * @return {[type]} [description]
   */
  getName() {
    return this.name;
  }
  /**
   * [getPreviousLessons description]
   * @return {[type]} [description]
   */
  getPreviousLessons() {
    return this.previous_lessons;
  }
  /**
   * [getQuestions description]
   * @return {[type]} [description]
   */
  getQuestions() {
    return this.questions;
  }
  /**
   * [getUuid description]
   * @return {[type]} [description]
   */
  getUuid() {
    return this.uuid;
  }
  /**
   * [update description]
   * @param  {[type]} data [description]
   */
  update(data) {
    for (const key of Object.keys(this)) {
      if (typeof data[key] !== 'undefined' && data[key] !== null) {
        this[key] = data[key];
      }
    }
  }
  /**
   * [getOverviewData description]
   * @return {[type]} [description]
   */
  getOverviewData() {
    return {
      approval_score: this.approval_score,
      description: this.description,
      name: this.name,
      uuid: this.uuid,
    };
  }
  /**
   * [getDetailedData description]
   * @return {[type]} [description]
   */
  getDetailedData() {
    const overviewData = this.getOverviewData();
    return {
      ...overviewData,
      previous_lessons: this.previous_lessons,
      questions: this.questions,
    };
  }
}

module.exports = Lesson;
