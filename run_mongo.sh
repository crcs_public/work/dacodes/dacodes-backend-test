#!/bin/bash

docker stop mongo || true
docker run -d --rm --name mongo --publish 27017:27017 mvertes/alpine-mongo
docker logs -f mongo

